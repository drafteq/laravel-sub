<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Playlist
 *
 * @package App
 *
 * @SWG\Definition(
 *   definition="Playlist",
 *   required={"name"}
 * )
 *
 */

class Playlist extends Model {
    protected $table = 'playlists';
    public $timestamps = false;

    protected $appends = ['playlistImageLink', 'tracksCount', 'playlistLink'];

    /**
     *
     *  @SWG\Property(property="uid", type="integer", required=true),
     *  @SWG\Property(property="by", type="integer", required=true),
     *  @SWG\Property(property="name", type="string", required=true),
     *  @SWG\Property(property="description", type="string", required=true),
     *  @SWG\Property(property="public", type="integer", required=true),
     *  @SWG\Property(property="time", type="date-time", required=true),
     *  @SWG\Property(property="playlistImageLink", type="string", required=false),
     */

    public function getPlaylistImageLinkAttribute() {
        $playlistLatestTrack = PlaylistEntry::where('playlist', $this->id)->orderBy('id', 'desc')->first();
        if(is_null($playlistLatestTrack))   return "";
        $track = Track::where('id', $playlistLatestTrack->track)->first();
        if(is_null($track))   return "";
        $trackDownloadLink = route('downloadTrackMedia', ['fileName' => $track->art]);
        return $trackDownloadLink;
    }

    public function getTracksCountAttribute() {
        $playlistTrackCount = PlaylistEntry::where('playlist', $this->id)->count();
        return "".$playlistTrackCount;
    }

    public function getPlaylistLinkAttribute() {
        return env('WEBSITE_BASE_URL')."index.php?a=playlist&id=$this->id";
    }


}
