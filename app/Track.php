<?php

namespace App;
use Carbon\Carbon;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use URL;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Track
 *
 * @package App
 *
 * @SWG\Definition(
 *   definition="Track",
 *   required={"name"}
 * )
 *
 */

class Track extends Model {
     
    protected $appends = ['trackDownloadLink', 'trackArtLink', 'trackLiked', 'trackVoted', 'uploadedBy'];

    protected $table = 'tracks';
    /**
     *
     *  @SWG\Property(property="id", type="string", required=true),
     *  @SWG\Property(property="uid", type="integer", required=true),
     *  @SWG\Property(property="title", type="string", required=true),
     *  @SWG\Property(property="description", type="string", required=true),
     *  @SWG\Property(property="name", type="string", required=true),
     *  @SWG\Property(property="tag", type="string", required=true),
     *  @SWG\Property(property="art", type="string", required=true),
     *  @SWG\Property(property="buy", type="string", required=true),
     *  @SWG\Property(property="record", type="string", required=true),
     *  @SWG\Property(property="release", type="string", format="date", required=true),
     *  @SWG\Property(property="license", type="integer", required=true),
     *  @SWG\Property(property="size", type="integer", required=true),
     *  @SWG\Property(property="download", type="integer", required=true),
     *  @SWG\Property(property="time", type="string", format="date-time", required=true),
     *  @SWG\Property(property="public", type="integer", required=true),
     *  @SWG\Property(property="likes", type="integer", required=true),
     *  @SWG\Property(property="downloads", type="integer", required=true),
     *  @SWG\Property(property="views", type="integer", required=true),
     *  @SWG\Property(property="embed", type="string", required=true),
     *  @SWG\Property(property="ratings", type="integer", required=true),
     *  @SWG\Property(property="trackDownloadLink", type="string", required=false),
     *  @SWG\Property(property="trackArtLink", type="string", required=false)
     *  @SWG\Property(property="trackLiked", type="string", required=false)
     *  @SWG\Property(property="trackVoted", type="integer", required=false)
     *  @SWG\Property(property="uploadedBy", type="string", required=false)
     */

    public $timestamps = false;
    
    public function comments() {
        return $this->hasMany('App\Comment', 'tid');
    }
    
    public function getTotalCommentsAttribute(){
        return $this->hasMany('App\Comment', 'tid')->where('tid',$this->id)->count();
    }

    public function getTrackDownloadLinkAttribute() {
        $trackDownloadLink = route('downloadTrack', ['fileName' => $this->name]);
        return $trackDownloadLink;
    }

    public function getTrackArtLinkAttribute() {
        $trackDownloadLink = route('downloadTrackMedia', ['fileName' => $this->art]);
        return $trackDownloadLink;
    }

    public function getTrackLikedAttribute() {
        try {
            $token = JWTAuth::parseToken();
            if ($user = $token->authenticate()) {
                $currentUserId = $user->idu;
                //User is logged in
                //Check if a entry exists in the Liked table for this user
                //Otherwise return false
                $liked = !is_null(Like::where('by', $currentUserId)->where('track', $this->id)->first());

                if($liked) {
                    return "1";
                }
            }
        } catch (JWTException $e) {
            return "0";
        }
        return "0";
    }

    public function getTrackVotedAttribute() {
        //User is not logged in / Not voted return 0
        //Voted and logged in then return the actual rating

        try {
            $token = JWTAuth::parseToken();
            if ($user = $token->authenticate()) {
                $currentUserId = $user->idu;
                //User is logged in
                //Only vip user can vote for the tracks
                //But that does not matter here because the user might have been a vip user when he was voting
                //So, basically check the vip_ratings table
                //If an entry exists there return that vote
                //otherwise return 0
                $rating = VipRating::where('by', $currentUserId)->where('track', $this->id)->first();
                if(is_null($rating)) {
                    //Haven't voted for the track
                    return "0";
                } else {
                    if($rating->rating > 0 && $rating->rating < 6) {
                        return $rating->rating;
                    } else {
                        return "0";
                    }
                }
            }
        } catch (JWTException $e) {
            return "0";
        }
        return "0";
    }

    public function getUploadedByAttribute() {
        $user = User::where('idu', $this->uid)->first();
        if(!is_null($user)) {
            return $user->first_name." ".$user->last_name;
        }
        return " ";
    }
}

