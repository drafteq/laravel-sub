<?php

namespace App;

use Carbon\Carbon;
use URL;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Track
 *
 * @package App
 *
 * @SWG\Definition(
 *   definition="Relation",
 *   required={"id"}
 * )
 *
 */
class Relation extends Model
{
    protected $table = 'relations';
    /**
     *
     *  @SWG\Property(property="id", type="integer", required=true),
     *  @SWG\Property(property="leader", type="integer", required=true),
     *  @SWG\Property(property="subscriber", type="integer", required=true),
     *  @SWG\Property(property="time", type="string", required=true),
     */

    public $timestamps = false;
}
