<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

use Swagger\Annotations as SWG;
use JWTAuth;

/**
 * Class User
 *
 * @package App
 *
 * @SWG\Definition(
 *   definition="User",
 *   required={"name"}
 * )
 *
 */



class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    /**
     *
     *  @SWG\Property(property="idu", type="integer", required=true),
     *  @SWG\Property(property="username", type="string", required=true),
     *  @SWG\Property(property="email", type="string", required=true),
     *  @SWG\Property(property="first_name", type="string", required=true),
     *  @SWG\Property(property="last_name", type="string", required=true),
     *  @SWG\Property(property="country", type="string", required=true),
     *  @SWG\Property(property="city", type="string", required=true),
     *  @SWG\Property(property="website", type="string", required=true),
     *  @SWG\Property(property="description", type="string", required=true),
     *  @SWG\Property(property="date", type="date", required=true),
     *  @SWG\Property(property="facebook", type="string", required=true),
     *  @SWG\Property(property="twitter", type="string", required=true),
     *  @SWG\Property(property="gplus", type="string", required=true),
     *  @SWG\Property(property="youtube", type="string", required=true),
     *  @SWG\Property(property="vimeo", type="string", required=true),
     *  @SWG\Property(property="tumblr", type="string", required=true),
     *  @SWG\Property(property="soundcloud", type="string", required=true),
     *  @SWG\Property(property="myspace", type="string", required=true),
     *  @SWG\Property(property="lastfm", type="string", required=true),
     *  @SWG\Property(property="image", type="string", required=true),
     *  @SWG\Property(property="private", type="integer", required=true),
     *  @SWG\Property(property="suspended", type="integer", required=true),
     *  @SWG\Property(property="salted", type="string", required=true),
     *  @SWG\Property(property="cover", type="string", required=true),
     *  @SWG\Property(property="gender", type="intger", required=true),
     *  @SWG\Property(property="online", type="int", required=true),
     *  @SWG\Property(property="ip", type="string", required=true),
     *  @SWG\Property(property="notificationl", type="integer", required=true),
     *  @SWG\Property(property="notificationc", type="integer", required=true),
     *  @SWG\Property(property="notificationd", type="integer", required=true),
     *  @SWG\Property(property="notificationf", type="integer", required=true),
     *  @SWG\Property(property="email_comment", type="integer", required=true),
     *  @SWG\Property(property="email_new_friend", type="integer", required=true),
     *  @SWG\Property(property="vip", type="integer", required=true),
     *  @SWG\Property(property="remember_token", type="string", required=true),
     *  @SWG\Property(property="hasProProfile", type="boolean", required=true),
     *  @SWG\Property(property="userFollowed", type="boolean", required=true),
     *  @SWG\Property(property="tracksCount", type="integer", required=true),
     *  @SWG\Property(property="followersCount", type="integer", required=true),
     *  @SWG\Property(property="followingsCount", type="integer", required=true),
     *  @SWG\Property(property="playlistsCount", type="integer", required=true),
     *  @SWG\Property(property="likesCount", type="integer", required=true),
     */


    /**
     * The database table used by the model.
     *
     * @var string
     */

    protected $primaryKey = 'idu';
    protected $table = 'users';
    protected $appends = ['hasProAccount',
        'coverLink',
        'profilePictureLink',
        'userFollowed',
        'tracksCount',
        'followersCount',
        'followingsCount',
        'playlistsCount',
        'likesCount',
        'unreadMessageCount',
        'userFriend',
        'userBlocked',
        'latestMessage',
        'latestMessageSender'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    public $timestamps = false;


    public function getHasProAccountAttribute() {
        $payment = Payment::where('by', $this->idu)
            ->orderBy('id', 'desc')
            ->first();

        if(is_null($payment)){
            return "0";
        }
        if(Carbon::create($payment->valid ) > Carbon::now()) {
            return "1";
        }
        return "0";
    }

    public function getCoverLinkAttribute() {
        $coverDownloadLink = route('downloadUserCover', ['fileName' => $this->cover]);
        return $coverDownloadLink;
    }

    public function getProfilePictureLinkAttribute() {
        $coverDownloadLink = route('downloadUserProfile', ['fileName' => $this->image]);
        return $coverDownloadLink;
    }

    public function getUserFollowedAttribute() {
        //User is not logged in / Not userFollowed return false
        //Followed and logged in then return true
        try {
            $token = JWTAuth::parseToken();
            if ($user = $token->authenticate()) {
                $currentUserId = $user->idu;
                //User is logged in
                //Check whether an entry exists in the relations table or not
                //otherwise return 0
                $following = Relation::where('subscriber', $currentUserId)->where('leader', $this->idu)->first();
                if(is_null($following)) {
                    //Not following
                    return "0";
                }
                return "1";
            }
        } catch (JWTException $e) {
            return "0";
        }
        return "0";
    }

    public function getUserFriendAttribute() {
        try {
            $token = JWTAuth::parseToken();
            if ($user = $token->authenticate()) {
                $currentUserId = $user->idu;
                //User is logged in
                //Check whether an entry exists in the relations table or not
                //otherwise return 0
                $following = Relation::where('subscriber', $currentUserId)->where('leader', $this->idu)->first();
                $follower = Relation::where('subscriber', $this->idu)->where('leader', $currentUserId)->first();
                if(is_null($following) || is_null($follower)) {
                    //Not following
                    return "0";
                }
                return "1";
            }
        } catch (JWTException $e) {
            return "0";
        }
        return "0";
    }

    public function getUserBlockedAttribute() {
        try {
            $token = JWTAuth::parseToken();
            if ($user = $token->authenticate()) {
                $currentUserId = $user->idu;
                //User is logged in
                //Check whether an entry exists in the relations table or not
                //otherwise return 0
                $blocked = Blocked::where('uid', $this->idu)->where('by', $currentUserId)->first();

                if(is_null($blocked)) {
                    //Not following
                    return "0";
                }
                return "1";
            }
        } catch (JWTException $e) {
            return "0";
        }
        return "0";
    }

    public function getFollowersCountAttribute() {
        return (string)Relation::where('leader', $this->idu)->count();
    }

    public function getTracksCountAttribute() {
        return (string)Track::where('uid', $this->idu)->count();
    }

    public function getFollowingsCountAttribute() {
        return (string)Relation::where('subscriber', $this->idu)->count();
    }

    public function getPlaylistsCountAttribute() {
        try {
            $token = JWTAuth::parseToken();
            if ($user = $token->authenticate()) {
                $currentUserId = $user->idu;

                if($currentUserId==$this->idu)
                    return (string)Playlist::where('by', $this->idu)->count();
            }
        } catch (JWTException $e) {
            return (string)Playlist::where('by', $this->idu)->where('public', 1)->count();
        }
        return (string)Playlist::where('by', $this->idu)->where('public', 1)->count();
    }

    public function getLikesCountAttribute() {
        $likedPublicTrackCount = Like::where('by', $this->idu)
            ->join('tracks', 'tracks.id', '=', 'likes.track')
            ->where('public', 1)
            ->count();
        return (string)$likedPublicTrackCount;
    }

    public function getUnreadMessageCountAttribute() {
        try {
            $token = JWTAuth::parseToken();
            if ($user = $token->authenticate()) {
                $currentUserId = $user->idu;
                //User is logged in
                //Check whether an entry exists in the relations table or not
                //otherwise return 0
                $unreadMessageCount = Chat::where('to', $currentUserId)
                    ->where('from', $this->idu)
                    ->where('read', 0)
                    ->count();
                return (string)$unreadMessageCount;
            }
        } catch (JWTException $e) {
            return "0";
        }
        return "0";
    }

    public function getLatestMessageAttribute() {
        try {
            $token = JWTAuth::parseToken();
            if ($user = $token->authenticate()) {
                $currentUserId = $user->idu;
                //Consider 2 cases - A and B
                //The current User is A
                //Check the chat history between A and B
                //If B's message to A is newer than the A's message to B
                //Then return B's latest message
                //otherwise return empty string
                $idList = [$this->idu, $currentUserId];
                $latestChat = Chat::whereIn('from', $idList)
                    ->whereIn('to', $idList)
                    ->orderBy('time', 'desc')
                    ->first();

                if(!is_null($latestChat) && $latestChat->from==$this->idu && $latestChat->to==$currentUserId) {
                    return $latestChat->message;
                }
                return "";
            }
        } catch (JWTException $e) {
            return "";
        }
        return "";
    }

    public function getLatestMessageSenderAttribute() {
        try {
            $token = JWTAuth::parseToken();
            if ($user = $token->authenticate()) {
                $currentUserId = $user->idu;
                //Consider 2 cases - A and B
                //The current User is A
                //Check the chat history between A and B
                //If B's message to A is newer than the A's message to B
                //Then return B's latest message
                //otherwise return empty string
                $idList = [$this->idu, $currentUserId];
                $latestChat = Chat::whereIn('from', $idList)
                    ->whereIn('to', $idList)
                    ->orderBy('time', 'desc')
                    ->first();

                if(!is_null($latestChat) && $latestChat->from==$this->idu && $latestChat->to==$currentUserId) {
                    return "other_user";
                }
                return "current_user";
            }
        } catch (JWTException $e) {
            return "no_one";
        }
        return "no_one";
    }

}
