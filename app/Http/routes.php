<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

header('Access-Control-Allow-Origin: http://docs.bcnmusicloud.tv');
//header('Access-Control-Allow-Origin: http://localhost');
header('Access-Control-Allow-Credentials: true');


Route::get('/', function () {
    return view('welcome');
});

//Route::get('/swagger', function () {
//    return view('vendor.swaggervel.index')->with('urlToDocs', 'abc');
//});

//Endpoints for the social logins

Route::group(['prefix' => 'v1'], function(){

    /***********************    Tracks related endpoints start here         ******************/
    
    //This is route is to get all the tracks and filters can be used according to the tags
    Route::get('/tracks/public', 'TrackController@indexPublicTrackList');
    Route::get('/tracks/latest', 'TrackController@getLatestTracks');
    Route::get('/tracks/popular', 'TrackController@getPopularTracks');
    Route::get('/tracks/liked', 'TrackController@getMostLikedTracks');
    Route::get('/tracks/stream', 'TrackController@getStreamTracks');
    Route::get('/tracks/categories', 'TrackController@getCategories');
    Route::get('/tracks/vip', 'TrackController@indexVipTrackList');
    // Route::get('/tracks/download/{fileName}',  'TrackController@downloadTrack');
    // Download Route
    // Not working when put inside the controller
    Route::get('/tracks/download/{filename}', ['as' => 'downloadTrack', function ($filename) {
        // Check if file exists in app/storage/file folder
        $file_path = storage_path() .'/app/tracks/'. $filename;
        if (file_exists($file_path)) {
            // Send Download
            return Response::download($file_path, $filename, [
                'Content-Length: '. filesize($file_path)
            ]);
        } else {
            return Response::json([
                'message' => 'Could not download the track.'
            ], 404);
        }
    }])->where('filename', '[A-Za-z0-9\-\_\.]+');
    Route::get('/tracks/art/download/{filename}', ['as' => 'downloadTrackMedia', function ($filename) {
        // Check if file exists in app/storage/file folder
        $file_path = storage_path() .'/app/media/'. $filename;
        if (file_exists($file_path)) {
            // Send Download
            return Response::download($file_path, $filename, [
                'Content-Length: '. filesize($file_path)
            ]);
        } else {
            return Response::json([
                'message' => 'Could not download the art.'
            ], 404);
        }
    }])->where('filename', '[A-Za-z0-9\-\_\.]+');
    Route::post('/tracks/{trackId}/likes', 'TrackController@likeTrack');
    Route::delete('/tracks/{trackId}/likes', 'TrackController@unLikeTrack');
    Route::post('/tracks/{trackId}/ratings', 'TrackController@rateTrack');
    Route::get('/tracks/{trackId}/comments', 'TrackController@getTrackComments');
    Route::post('/tracks/{trackId}/comments', 'TrackController@addTrackComment');
    Route::delete('/tracks/{trackId}/comments/{commentId}', 'TrackController@deleteTrackComment');
    Route::get('/tracks', 'TrackController@listTracks');
    Route::get('/tracks/{trackId}', 'TrackController@getTrack');
    Route::get('/me/tracks', 'UserController@getSelfTracks');
    Route::get('/me/tracks/liked', 'UserController@getSelfLikedTracks');
    Route::post('/me/tracks', 'TrackController@uploadTrack');
    Route::get('me/tracks/{trackId}', 'TrackController@getSelfTrack');
    Route::delete('me/tracks/{trackId}', 'TrackController@deleteTrack');
    Route::post('me/tracks/{trackId}', 'TrackController@updateTrack');



    /***********************    Tracks related endpoints end here         ******************/

    //User Related Endpoints
    //Route::post('/authenticate', 'AuthenticateController@authenticate');
    Route::post('/auth/token', 'AuthenticateController@authenticate');
    Route::delete('/auth/token', 'AuthenticateController@logout');
    Route::post('/auth/token/refresh', 'AuthenticateController@refreshToken');
    Route::post('/auth/token/social', 'UserController@socialLogin');
    Route::get('/me', 'AuthenticateController@getCurrentUser');
    Route::post('/users', 'AuthenticateController@registerUser');

    Route::get('/me/statistics', 'UserController@getUserStatistics');
    Route::post('/settings/general', 'UserController@updateUserGeneralSettings');
    Route::post('/settings/notifications', 'UserController@updateUserNotificationSettings');
    Route::post('/settings/social', 'UserController@updateUserSocialProfileSettings');
    Route::post('/settings/password', 'UserController@updateUserPassword');
    Route::post('/settings/profile_picture', 'UserController@updateUserProfilePicture');
    Route::post('/settings/cover_picture', 'UserController@updateUserCoverPicture');
    
    //User follower related endpoints
    Route::post('/users/{userId}/follows', 'UserController@follow');
    Route::delete('/users/{userId}/follows', 'UserController@unfollow');

    Route::get('/users/{userId}', 'UserController@getUser');
    Route::get('/users/{userId}/followers', 'UserController@getUserFollowers');
    Route::get('/users/{userId}/followings', 'UserController@getUserFollowing');
    Route::get('/users/{userId}/tracks', 'UserController@getUserPublicTracks');
    Route::get('/users/{userId}/tracks/liked', 'UserController@getUserLikedTracks');
    Route::get('/users/{userId}/playlists', 'UserController@getUserPlaylists');

    Route::get('/me', 'AuthenticateController@getCurrentUser');
    Route::get('/me/online', 'UserController@updateSelfOnlineStatus');
    Route::get('/me/friends', 'UserController@getFriends');
    Route::get('/me/messages/count', 'UserController@getSelfUnreadMessageCount');
    Route::get('/me/followers', 'UserController@getSelfFollowers');
    Route::get('/me/followings', 'UserController@getSelfFollowing');
    Route::post('/me/password_reset', 'UserController@resetPasswordRequest');
    
    Route::get('/me/notifications', 'UserController@getNotifications');
    Route::post('/me/notifications/{notificationId}/read', 'UserController@readNotification');
    
    
    Route::get('/me/playlists', 'UserController@getPlaylists');
    Route::post('/me/playlists', 'UserController@addPlaylist');
    Route::delete('/me/playlists/{playlistId}', 'UserController@removePlaylist');
    Route::post('/me/playlists/{playlistId}', 'UserController@editPlaylist');
    Route::post('/me/playlists/{playlistId}/tracks/{trackId}', 'UserController@addTrackToPlaylist');
    Route::delete('/me/playlists/{playlistId}/tracks/{trackId}', 'UserController@removeTrackFromPlaylist');
    Route::get('/me/playlists/{playlistId}/tracks', 'UserController@getSelfTracksOfThePlaylist');
    Route::get('/users/playlists/{playlistId}/tracks', 'UserController@getTracksOfThePlaylist');


    Route::get('users/{userId}/messages', 'UserController@getChats');
    Route::post('users/{userId}/messages/{messageId}/read/all', 'UserController@readAllChats');
    Route::post('users/{userId}/messages', 'UserController@sendMessage');
    Route::post('users/{userId}/blocks', 'UserController@blockUser');
    Route::delete('users/{userId}/blocks', 'UserController@unBlockUser');


    Route::get('/search', 'UserController@search');
    Route::get('/search/users', 'UserController@searchUsers');
    Route::get('/search/tracks', 'UserController@searchTracks');
    Route::get('/search/playlists', 'UserController@searchPlaylists');


    Route::get('/cover/download/{filename}', ['as' => 'downloadUserCover', function ($filename) {
        // Check if file exists in app/storage/file folder
        $file_path = storage_path() .'/app/covers/'. $filename;
        if (file_exists($file_path)) {
            // Send Download
            return Response::download($file_path, $filename, [
                'Content-Length: '. filesize($file_path)
            ]);
        } else {
            return Response::json([
                'message' => 'Could not download the cover.'
            ], 404);
        }
    }])->where('filename', '[A-Za-z0-9\-\_\.]+');
    Route::get('/profilePicture/download/{filename}', ['as' => 'downloadUserProfile', function ($filename) {
        // Check if file exists in app/storage/file folder
        $file_path = storage_path() .'/app/avatars/'. $filename;
        if (file_exists($file_path)) {
            // Send Download
            return Response::download($file_path, $filename, [
                'Content-Length: '. filesize($file_path)
            ]);
        } else {
            return Response::json([
                'message' => 'Could not download the profile picture.'
            ], 404);
        }
    }])->where('filename', '[A-Za-z0-9\-\_\.]+');


    //Payment related endpoint
//    Route::post('/resetPasswordProcess', 'UserController@resetPasswordRequestProcess');
    /** Deprecated Endpoints */
//    Route::post('/register', 'UserController@registerUser');
    //    Route::post('payViaPaypal', 'PaymentController@postPayment');
    //Route::get('/playedMost', 'UserController@playedMost');
    //Route::get('/downloadedMost', 'UserController@downloadedMost');

});
