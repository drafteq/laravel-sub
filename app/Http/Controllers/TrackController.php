<?php

namespace App\Http\Controllers;

use App\Blocked;
use App\Category;
use App\Comment;
use App\Http\Requests\TrackLikeRequest;
use App\Like;
use App\Notification;
use App\Relation;
use App\Track;
use App\User;
use App\VipRating;
use Carbon\Carbon;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Input;
use JWTAuth;
use Response;
use Tymon\JWTAuth\Exceptions\JWTException;


class TrackController extends Controller {

    public function __construct() {
        $this->middleware('jwt.auth', ['except' => [
            'indexPublicTrackList',
            'indexVipTrackList',
            'getPopularTracks',
            'getLatestTracks',
            'listTracks',
            'getMostLikedTracks',
            'getCategories',
            'getTrackComments',
            'getTrack'
            ]
        ]);
    }

    /**
     * @SWG\Get(
     *     path="/tracks?limit={limit}&page={page}",
     *     summary="Lists all tracks corresponding to the explore page",
     *     tags={"tracks"},
     *     description="Lists all tracks corresponding to the explore page",
     *     operationId="listTracks",
     *     parameters={},
     *     produces={"application/json"},
     *     @SWG\Parameter(name="limit", in="path", type="integer", required=false, description="Provide this value to limit the results, this is optional in actual API Request", maxLength=256, default=10),
     *     @SWG\Parameter(name="page", in="path", type="integer", required=false, description="Provide this value to get the results based on the page number", maxLength=256, default=1),
     *     @SWG\Parameter(name="categoryId", in="formData", type="string", required=false, description="Optional Category Id to filter the songs", maxLength=256),
     *     @SWG\Response(
     *         response=200,
     *         description="List of all the tracks based on the optional tag",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="total", type="integer", description="Total Number of Results"),
     *             @SWG\Property(property="per_page", type="integer", description="Results per page"),
     *             @SWG\Property(property="current_page", type="integer", description="Current Page Number"),
     *             @SWG\Property(property="last_page", type="integer", description="Last Page Number"),
     *             @SWG\Property(property="next_page_url", type="string", description="Next Result Page URL"),
     *             @SWG\Property(property="prev_page_url", type="string", description="Prev Result Page URL"),
     *             @SWG\Property(property="from", type="integer", description="Current Offset"),
     *             @SWG\Property(property="to", type="integer", description="Current Range"),
     *             @SWG\Property(property="data", type="array", description="List of Tracks",  @SWG\Items(ref="#/definitions/Track")),
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="404",
     *         description="Could Not Retrieve Tracks",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="message", type="string", description="no_tracks_found"),
     *          )
     *     )
     * )
     * @param Requests\TrackListRequest $request
     * @return \Illuminate\Http\JsonResponse
     */

    public function listTracks(Requests\TrackListRequest $request){
        $limit = Input::get('limit')?:10;
        $attributes = $request->all();
        $query = Track::orderBy('time', 'desc');
        if(array_key_exists('categoryId', $attributes)) {
            $category = Category::where('id', $attributes['categoryId'])->first();
            $query = (!is_null($category) ?
                $query->where('tag', 'LIKE', '%'.str_replace(' ', '', strtolower($category->name)).'%') : $query);
        }
        return $this->respondWithTracks($query->paginate($limit), "no_tracks_found");
    }

    /**
     * @SWG\Get(
     *     path="/tracks/public?limit={limit}&page={page}",
     *     summary="Finds track based on public popularity",
     *     tags={"tracks"},
     *     description="List the tracks based on the public popularity",
     *     operationId="indexPublicTrackList",
     *     parameters={},
     *     produces={"application/json"},
     *     @SWG\Parameter(name="limit", in="path", type="integer", required=false, description="Provide this value to limit the results, this is optional in actual API Request", maxLength=256, default=10),
     *     @SWG\Parameter(name="page", in="path", type="integer", required=false, description="Provide this value to get the results based on the page number", maxLength=256, default=1),
     *     @SWG\Response(
     *         response=200,
     *         description="List of the tracks based on the public popularity",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="total", type="integer", description="Total Number of Results"),
     *             @SWG\Property(property="per_page", type="integer", description="Results per page"),
     *             @SWG\Property(property="current_page", type="integer", description="Current Page Number"),
     *             @SWG\Property(property="last_page", type="integer", description="Last Page Number"),
     *             @SWG\Property(property="next_page_url", type="string", description="Next Result Page URL"),
     *             @SWG\Property(property="prev_page_url", type="string", description="Prev Result Page URL"),
     *             @SWG\Property(property="from", type="integer", description="Current Offset"),
     *             @SWG\Property(property="to", type="integer", description="Current Range"),
     *             @SWG\Property(property="data", type="array", description="List of Tracks",  @SWG\Items(ref="#/definitions/Track")),
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="404",
     *         description="Could Not Retrieve Tracks",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="message", type="string", description="no_tracks_found"),
     *          )
     *     )
     * )
     */

    public function indexPublicTrackList() {
        $limit = Input::get('limit')?:10;
        $tracks = Track::orderBy('likes', 'desc')->where('public', 1)->paginate($limit);
        return $this->respondWithTracks($tracks, "no_tracks_found");
    }

    /**
     * @SWG\Get(
     *     path="/tracks/vip?limit={limit}&page={page}",
     *     summary="Finds track based on VIP ratings",
     *     tags={"tracks"},
     *     description="List the tracks based on the vip ratings",
     *     operationId="indexVipTrackList",
     *     parameters={},
     *     produces={"application/json"},
     *     @SWG\Parameter(name="limit", in="path", type="integer", required=false, description="Provide this value to limit the results, this is optional in actual API Request", maxLength=256, default=10),
     *     @SWG\Parameter(name="page", in="path", type="integer", required=false, description="Provide this value to get the results based on the page number", maxLength=256, default=1),
     *     @SWG\Response(
     *         response=200,
     *         description="List of the tracks based on the public popularity",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="total", type="integer", description="Total Number of Results"),
     *             @SWG\Property(property="per_page", type="integer", description="Results per page"),
     *             @SWG\Property(property="current_page", type="integer", description="Current Page Number"),
     *             @SWG\Property(property="last_page", type="integer", description="Last Page Number"),
     *             @SWG\Property(property="next_page_url", type="string", description="Next Result Page URL"),
     *             @SWG\Property(property="prev_page_url", type="string", description="Prev Result Page URL"),
     *             @SWG\Property(property="from", type="integer", description="Current Offset"),
     *             @SWG\Property(property="to", type="integer", description="Current Range"),
     *             @SWG\Property(property="data", type="array", description="List of Tracks",  @SWG\Items(ref="#/definitions/Track")),
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="404",
     *         description="Could Not Retrieve Tracks",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="message", type="string", description="no_tracks_found"),
     *          )
     *     )
     * )
     */
    public function indexVipTrackList() {
        $limit = Input::get('limit')?:10;
        $staffTracks = Track::join('vip_rating', 'vip_rating.track', '=', 'tracks.id')
            ->join('users', 'users.idu', '=', 'tracks.uid')
            ->selectRaw('tracks.*, avg(rating) as averageRating, COUNT(track) as ratingsCount')
            ->groupBy('track')
            ->orderBy('averageRating', 'DESC')
            ->orderBy('ratingsCount', 'DESC')
            ->orderBy('id', 'DESC')
            ->where('users.private', '0');

        try {
            $token = JWTAuth::parseToken();
            if ($user = $token->authenticate()) {
                $staffTracks = $staffTracks->orWhere('public', '=' ,'1')
                    ->orWhere('tracks.uid', '=', $user->idu);
            } else {
                $staffTracks = $staffTracks->orWhere('public', '=' ,'1');
            }
        } catch (JWTException $e) {
            $staffTracks = $staffTracks->orWhere('public', '=' ,'1');
        }

//        dd($staffTracks->getQuery()->toSql());
        $staffTracks = $staffTracks->paginate($limit);

        return $this->respondWithTracks($staffTracks, "could_not_retrieve_tracks");
    }

    /**
     * @SWG\Get(
     *     path="/tracks/latest?limit={limit}&page={page}",
     *     summary="Finds recent tracks",
     *     tags={"tracks"},
     *     description="Finds Recent tracks",
     *     operationId="indexPublicTrackList",
     *     parameters={},
     *     produces={"application/json"},
     *     @SWG\Parameter(name="limit", in="path", type="integer", required=false, description="Provide this value to limit the results, this is optional in actual API Request", maxLength=256, default=10),
     *     @SWG\Parameter(name="page", in="path", type="integer", required=false, description="Provide this value to get the results based on the page number", maxLength=256, default=1),
     *     @SWG\Response(
     *         response=200,
     *         description="List of recent tracks",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="total", type="integer", description="Total Number of Results"),
     *             @SWG\Property(property="per_page", type="integer", description="Results per page"),
     *             @SWG\Property(property="current_page", type="integer", description="Current Page Number"),
     *             @SWG\Property(property="last_page", type="integer", description="Last Page Number"),
     *             @SWG\Property(property="next_page_url", type="string", description="Next Result Page URL"),
     *             @SWG\Property(property="prev_page_url", type="string", description="Prev Result Page URL"),
     *             @SWG\Property(property="from", type="integer", description="Current Offset"),
     *             @SWG\Property(property="to", type="integer", description="Current Range"),
     *             @SWG\Property(property="data", type="array", description="List of Tracks",  @SWG\Items(ref="#/definitions/Track")),
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="404",
     *         description="Could Not Retrieve Tracks",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="message", type="string", description="could_not_retrieve_tracks"),
     *          )
     *     )
     * )
     */

    public function getLatestTracks() {
        $limit = Input::get('limit')?:10;
        $latestTracks = Track::orderBy('id', 'desc')->where('public', '1')->paginate($limit);
        return $this->respondWithTracks($latestTracks, "could_not_retrieve_tracks");
    }

    /**
     * @SWG\Get(
     *     path="/tracks/popular?limit={limit}&page={page}",
     *     summary="Finds Popular tracks",
     *     tags={"tracks"},
     *     description="Finds Popular tracks",
     *     operationId="popular",
     *     parameters={},
     *     produces={"application/json"},
     *     @SWG\Parameter(name="limit", in="path", type="integer", required=false, description="Provide this value to limit the results, this is optional in actual API Request", maxLength=256, default=10),
     *     @SWG\Parameter(name="page", in="path", type="integer", required=false, description="Provide this value to get the results based on the page number", maxLength=256, default=1),
     *     @SWG\Response(
     *         response=200,
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="total", type="integer", description="Total Number of Results"),
     *             @SWG\Property(property="per_page", type="integer", description="Results per page"),
     *             @SWG\Property(property="current_page", type="integer", description="Current Page Number"),
     *             @SWG\Property(property="last_page", type="integer", description="Last Page Number"),
     *             @SWG\Property(property="next_page_url", type="string", description="Next Result Page URL"),
     *             @SWG\Property(property="prev_page_url", type="string", description="Prev Result Page URL"),
     *             @SWG\Property(property="from", type="integer", description="Current Offset"),
     *             @SWG\Property(property="to", type="integer", description="Current Range"),
     *             @SWG\Property(property="data", type="array", description="List of Tracks",  @SWG\Items(ref="#/definitions/Track")),
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="404",
     *         description="Could Not Retrieve Tracks",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="message", type="string", description="could_not_retrieve_tracks"),
     *          )
     *     )
     * )
     */

    public function getPopularTracks() {
        $limit = Input::get('limit')?:10;

        $popularTracks = Track::selectRaw('tracks.*, COUNT(track) as viewsCount')
            ->join('views', 'views.track', '=', 'tracks.id')
            ->join('users', 'users.idu', '=', 'tracks.uid')
            ->groupBy('track')
            ->orderBy('viewsCount', 'DESC')
            ->where('users.private', '0');

        try {
            $token = JWTAuth::parseToken();
            if ($user = $token->authenticate()) {
                $popularTracks = $popularTracks->orWhere('public', '=' ,'1')->orWhere('tracks.uid', '=', $user->idu);
            } else {
                $popularTracks = $popularTracks->orWhere('public', '=' ,'1');
            }
        } catch (JWTException $e) {
            $popularTracks = $popularTracks->orWhere('public', '=' ,'1');
        }
        $popularTracks = $popularTracks->paginate($limit);
        return $this->respondWithTracks($popularTracks, "could_not_retrieve_tracks");
    }

    /**
     * @SWG\Get(
     *     path="/tracks/liked?limit={limit}&page={page}",
     *     summary="Finds Most Liked tracks",
     *     tags={"tracks"},
     *     description="Finds Most Liked tracks",
     *     operationId="liked",
     *     parameters={},
     *     produces={"application/json"},
     *     @SWG\Parameter(name="limit", in="path", type="integer", required=false, description="Provide this value to limit the results, this is optional in actual API Request", maxLength=256, default=10),
     *     @SWG\Parameter(name="page", in="path", type="integer", required=false, description="Provide this value to get the results based on the page number", maxLength=256, default=1),
     *     @SWG\Response(
     *         response=200,
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="total", type="integer", description="Total Number of Results"),
     *             @SWG\Property(property="per_page", type="integer", description="Results per page"),
     *             @SWG\Property(property="current_page", type="integer", description="Current Page Number"),
     *             @SWG\Property(property="last_page", type="integer", description="Last Page Number"),
     *             @SWG\Property(property="next_page_url", type="string", description="Next Result Page URL"),
     *             @SWG\Property(property="prev_page_url", type="string", description="Prev Result Page URL"),
     *             @SWG\Property(property="from", type="integer", description="Current Offset"),
     *             @SWG\Property(property="to", type="integer", description="Current Range"),
     *             @SWG\Property(property="data", type="array", description="List of Tracks",  @SWG\Items(ref="#/definitions/Track")),
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="404",
     *         description="Could Not Retrieve Tracks",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="message", type="string", description="could_not_retrieve_tracks"),
     *          )
     *     )
     * )
     */

    public function getMostLikedTracks() {
        $limit = Input::get('limit')?:10;

        $mostLikedTracks = Track::selectRaw('tracks.*, COUNT(track) as likesCount')
            ->join('likes', 'likes.track', '=', 'tracks.id')
            ->join('users', 'users.idu', '=', 'tracks.uid')
            ->groupBy('track')
            ->orderBy('likesCount', 'DESC')
            ->where('users.private', '0');

        try {
            $token = JWTAuth::parseToken();
            if ($user = $token->authenticate()) {
                $mostLikedTracks = $mostLikedTracks->orWhere('public', '=' ,'1')
                    ->orWhere('tracks.uid', '=', $user->idu);
            } else {
                $mostLikedTracks = $mostLikedTracks->orWhere('public', '=' ,'1');
            }
        } catch (JWTException $e) {
            $mostLikedTracks = $mostLikedTracks->orWhere('public', '=' ,'1');
        }

        $mostLikedTracks = $mostLikedTracks->paginate($limit);

        return $this->respondWithTracks($mostLikedTracks, "could_not_retrieve_tracks");
    }

    /**
     * @SWG\Get(
     *     path="/tracks/stream?token={token}&limit={limit}&page={page}",
     *     summary="Finds all tracks",
     *     tags={"tracks"},
     *     description="Finds all tracks corresponding to stream page",
     *     operationId="streamTracks",
     *     parameters={},
     *     produces={"application/json"},
     *     @SWG\Parameter(name="limit", in="path", type="integer", required=false, description="Provide this value to limit the results, this is optional in actual API Request", maxLength=256, default=10),
     *     @SWG\Parameter(name="page", in="path", type="integer", required=false, description="Provide this value to get the results based on the page number", maxLength=256, default=1),
     *     @SWG\Parameter(name="token", in="path", type="string", required=true, description="Provide the access token of the user", maxLength=256),
     *     @SWG\Response(
     *         response=200,
     *         description="List of all the tracks based on the optional tag",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="total", type="integer", description="Total Number of Results"),
     *             @SWG\Property(property="per_page", type="integer", description="Results per page"),
     *             @SWG\Property(property="current_page", type="integer", description="Current Page Number"),
     *             @SWG\Property(property="last_page", type="integer", description="Last Page Number"),
     *             @SWG\Property(property="next_page_url", type="string", description="Next Result Page URL"),
     *             @SWG\Property(property="prev_page_url", type="string", description="Prev Result Page URL"),
     *             @SWG\Property(property="from", type="integer", description="Current Offset"),
     *             @SWG\Property(property="to", type="integer", description="Current Range"),
     *             @SWG\Property(property="data", type="array", description="List of Tracks",  @SWG\Items(ref="#/definitions/Track")),
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="404",
     *         description="Could Not Retrieve Tracks",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="message", type="string", description="could_not_retrieve_tracks"),
     *          )
     *     )
     * )
     */
    public function getStreamTracks() {
        $limit = Input::get('limit')?:10;
        $user = JWTAuth::parseToken()->authenticate();
        $currentUserId = $user->idu;
        $currentUserFriendsList = Relation::where('subscriber', $currentUserId)->lists('leader')->all();
        $currentBlockedFriends = Blocked::where('by', $currentUserId)->lists('uid')->all();
        $friendsIdList = array_diff($currentUserFriendsList, $currentBlockedFriends);
        array_push($friendsIdList, $currentUserId);
        $streamTracks = Track::whereIn('uid', $friendsIdList)->paginate($limit);
        return $this->respondWithTracks($streamTracks, "could_not_retrieve_tracks");
    }

    /**
     * @SWG\Get(
     *     path="/tracks/categories?limit{limit}",
     *     summary="Lists all the categories",
     *     tags={"tracks"},
     *     description="Lists all the categories available for the tracks",
     *     operationId="categories",
     *     parameters={},
     *     @SWG\Parameter(name="limit", in="path", type="integer", required=false, default=10, description="Limit the number of results", maxLength=256),
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="List of all the tracks based on the optional tag",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="total", type="integer", description="Total Number of Results"),
     *             @SWG\Property(property="per_page", type="integer", description="Results per page"),
     *             @SWG\Property(property="current_page", type="integer", description="Current Page Number"),
     *             @SWG\Property(property="last_page", type="integer", description="Last Page Number"),
     *             @SWG\Property(property="next_page_url", type="string", description="Next Result Page URL"),
     *             @SWG\Property(property="prev_page_url", type="string", description="Prev Result Page URL"),
     *             @SWG\Property(property="from", type="integer", description="Current Offset"),
     *             @SWG\Property(property="to", type="integer", description="Current Range"),
     *             @SWG\Property(property="data", type="array", description="List of Categories",  @SWG\Items(ref="#/definitions/Category")),
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="404",
     *         description="Could Not Retrieve Categories",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="message", type="string", description="could_not_retrieve_categories"),
     *          )
     *     )
     * )
     */

    public function getCategories() {
        $limit = Input::get('limit')?:10;
        $trackCategories = Category::paginate($limit);
        if(is_null($trackCategories)) return $this->respondWithMessage("could_not_retrieve_categories", 404);
        return $this->respondWithData($trackCategories->toArray());
    }

    /**
     * @SWG\Post(
     *     path="/me/tracks?token={token}",
     *     summary="Upload a track",
     *     tags={"tracks"},
     *     description="Upload a track",
     *     operationId="upload",
     *     parameters={},
     *     produces={"application/json"},
     *     @SWG\Parameter(name="token", in="path", type="string", required=true, description="Provide the access token of the user", maxLength=256),
     *     @SWG\Parameter(name="title", in="body", type="string", required=true, description="Provide the title of the track", maxLength=256),
     *     @SWG\Parameter(name="description", in="body", type="string", required=false, description="Provide the description about the track", maxLength=256),
     *     @SWG\Parameter(name="name", in="body", type="string", required=true, description="Provide the description about the track", maxLength=256),
     *     @SWG\Parameter(name="tag", in="body", type="string", required=true, description="Provide the tag for the track", maxLength=256),
     *     @SWG\Parameter(name="image", in="body", type="file", required=false, description="Provide the art for the track", maxLength=256),
     *     @SWG\Parameter(name="track", in="body", type="file", required=true, description="Provide the track", maxLength=256),
     *     @SWG\Parameter(name="buyLink", in="body", type="string", required=false, description="Provide the link to buy the track", maxLength=256),
     *     @SWG\Parameter(name="recordLabel", in="body", type="string", required=false, description="Provide the record label for the track", maxLength=256),
     *     @SWG\Parameter(name="license", in="body", type="integer", required=false, description="Provide the license for the track", maxLength=256),
     *     @SWG\Parameter(name="visibility", in="body", type="integer", required=false, description="Provide the visibility for the track", maxLength=256),
     *     @SWG\Parameter(name="embed", in="body", type="string", required=false, description="Provide the embed link for the track", maxLength=256),
     *     @SWG\Response(
     *         response=200,
     *         description="Track uploaded successfully",
     *         @SWG\Schema(
     *             type="object",
     *             ref="#/definitions/Track"
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="Track Missing | Exceeded Space",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="message", type="string", description="track_missing | track_size_exceeds_available_space"),
     *          )
     *     )
     * )
     * @param Requests\TrackUploadRequest $request
     * @return \Illuminate\Http\JsonResponse
     */

    public function uploadTrack(Requests\TrackUploadRequest $request){
        $user = JWTAuth::parseToken()->authenticate();
        $currentUserId = $user->idu;
        $attributes = $request->all();
        $fileImage = $request->file('image');
        $fileTrack = $request->file('track');
        if(is_null($fileTrack)) return $this->respondWithMessage("track_missing", 400);
        $currentTrackSize = $fileTrack->getClientSize();
        $currentTrackSizeInMb = $currentTrackSize/(1024 * 1024);
        $currentTime = Carbon::now();
        $currentSpaceInUse = Track::where('uid', $currentUserId)->sum('size');
        $currentSpaceInUseInMb = $currentSpaceInUse/(1024 * 1024);
        $maxTrackSizeAllowed = $this->getMaxTrackSizeAllowed($user->hasProAccount);
        $maxSizeAllowed = $this->getMaxSpaceAvailable($user->hasProAccount);
        $allowTrackUpload = ($currentTrackSizeInMb + $currentSpaceInUseInMb <= $maxSizeAllowed && $maxTrackSizeAllowed >= $currentTrackSizeInMb);
        if($allowTrackUpload){
            $track  = new Track();
            $track->uid = $currentUserId;
            $trackName = $currentTime->timestamp."-".md5($currentTime->timestamp)."-".$currentUserId;
            $track->name = $trackName.".".$fileTrack->getClientOriginalExtension();
            $track = $this->setTrackAttributes($track, $attributes);
            $track = $this->resetTrackCounters($track);
            $track->art = (!is_null($fileImage))? $trackName.".".$fileImage->getClientOriginalExtension():'default.png';
            $track->size = $currentTrackSize;
            $track->time = $currentTime;
            $fileTrack->move(storage_path().'/app/tracks/', $track->name);

            $copyrightInfo = $this->checkForCopyRight($track->name);

            if($copyrightInfo!=1) {
                if(unlink(storage_path().'/app/tracks/'.$track->name)) {
                    unlink(storage_path().'/app/sample/'.$track->name);
                    return $this->respondWithData($copyrightInfo, 400);
                }
            }
            if(!is_null($fileImage)) {
                $pathName = storage_path() . '/app/';
                $fileImage->move($pathName.'media_uncompressed/', $track->art);
                $this->compressAndSaveImage($pathName .'media_uncompressed/'.$track->art, $pathName .'media/'.$track->art);
            }
            $track->save();
            return $this->respondWithData($track);
        }
        return $this->respondWithMessage("track_size_exceeds_available_space", 400);
    }


    public function updateTrack($trackId, Request $request){
        $user = JWTAuth::parseToken()->authenticate();
        $currentUserId = $user->idu;
        $attributes = $request->all();
        $fileImage = $request->file('image');
        $currentTime = Carbon::now();
        $track  = Track::where('id', $trackId)->first();
        if(!is_null($track)){
            $track->time = $currentTime;
            $track = $this->setTrackAttributes($track, $attributes);
            if(!is_null($fileImage))    {
                $uniqueName = $currentTime->timestamp."-".md5($currentTime->timestamp)."-".$currentUserId;
                $track->art = $uniqueName .".".$fileImage->getClientOriginalExtension();
                $fileImage->move(storage_path().'/app/media/', $track->art);
            }
            $track->save();
            return $this->respondWithData($track);
        }
        return $this->respondWithMessage("could_not_update_the_track", 400);
    }

    /**
     * @SWG\Post(
     *     path="/tracks/{trackId}/likes?token={token}",
     *     summary="Like a track",
     *     tags={"tracks"},
     *     description="Like a track",
     *     operationId="like",
     *     parameters={},
     *     produces={"application/json"},
     *     @SWG\Parameter(name="token", in="path", type="string", required=true, description="Provide the access token of the user", maxLength=256),
     *     @SWG\Parameter(name="trackId", in="path", type="integer", required=true, description="Provide the id of the track to like", maxLength=256),
     *     @SWG\Response(
     *         response=200,
     *         description="Track liked successfully",
     *         @SWG\Schema(
     *             type="object",
     *             ref="#/definitions/Like"
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="403",
     *         description="Could not like the track",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="message", type="string", description="track_not_found | track_already_liked"),
     *          )
     *     )
     * )
     * @param $trackId
     * @return \Illuminate\Http\JsonResponse
     * @internal param TrackLikeRequest $request
     */
    public function likeTrack($trackId){
        $user = JWTAuth::parseToken()->authenticate();
        $currentUserId = $user->idu;
        $track = Track::where('id', $trackId)->first();
        $isLiked = Like::where('by', $currentUserId)->where('track', $trackId)->first();
        if(!is_null($isLiked)) {
            $isLiked->delete();
            if(is_null($track))  return $this->respondWithMessage("track_not_found", 403);
            $track->likes = $track->likes - 1;
            $track->save();
            return $this->respondWithMessage("un-liked_successful", 200);
        }
        if(is_null($track)) return $this->respondWithMessage("track_not_found", 403);
        $track->likes = $track->likes+1;
        $track->save();
        $like = new Like();
        $like->by = $currentUserId;
        $like->track = $trackId;
        $like->time = Carbon::now();
        $like->save();

        //Create a notification resource also
        $notification = new Notification();
        $notification->read = 0;
        $notification->parent = $currentUserId;
        $notification->type = 2;
        $notification->child = $track->id;
        $notification->from = $currentUserId;
        $notification->to = $track->uid;
        $notification->save();
            
        return $this->respondWithMessage("liked_successful", 200);
    }

    /**
     * @SWG\Delete(
     *     path="/tracks/{trackId}/likes?token={token}",
     *     summary="UnLike a track",
     *     tags={"tracks"},
     *     description="Unlike a track",
     *     operationId="unlike",
     *     parameters={},
     *     produces={"application/json"},
     *     @SWG\Parameter(name="trackId", in="body", type="integer", required=true, description="Provide the id of the track to like", maxLength=256),
     *     @SWG\Parameter(name="token", in="path", type="string", required=true, description="Provide the access token of the user", maxLength=256),
     *     @SWG\Response(
     *         response="200",
     *         description="Track Unliked",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="message", type="string", description="un-liked_successful"),
     *          )
     *     ),
     *     @SWG\Response(
     *         response="403",
     *         description="Track Not Found | Track Not Liked",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="message", type="string", description="un-track_not_found | track_not_liked "),
     *          )
     *     )
     * )
     * @param $trackId
     * @return \Illuminate\Http\JsonResponse
     */
    public function unLikeTrack($trackId){
        $user = JWTAuth::parseToken()->authenticate();
        $currentUserId = $user->idu;
        $like = Like::where('track', $trackId)->where('by', $currentUserId)->first();
        if(is_null($like))  return $this->respondWithMessage("track_not_liked", 403);
        $like->delete();

        $track = Track::where('id', $trackId)->first();
        if(is_null($track))  return $this->respondWithMessage("track_not_found", 403);
        $track->likes = $track->likes - 1;
        $track->save();
        return $this->respondWithMessage("un-liked_successful", 200);

    }

    public function getTrackComments($trackId) {
        //If the track is public then any one can get the comments
        //Otherwise only the owner can get the comments
        //If the user has unique url to the track then he also can see the comments
        //For now this api just focuses on retrieveing comment considering public/private and requester details

        $track = Track::where('id', $trackId)->first();
        if(is_null($track)) return $this->respondWithMessage('could_not_find_track', 404);

        //1 indicates Public / 0 indicates Private
        $isPublic = $track->public;
        $limit = Input::get("limit")?:10;

        $user = JWTAuth::parseToken()->authenticate();
        $currentUserId = $user->idu;

        //Track is public or Caller is the track owner then retrieve the comments
        if($isPublic==1 || $currentUserId==$track->uid) {
            $comments = Comment::where('tid', $trackId)->orderBy('time', 'desc')->with('byUser')->paginate($limit);
            if (is_null($comments)) return $this->respondWithMessage('could_not_retrieve_comments', 404);
            return $this->respondWithData($comments);
        }

        return $this->respondWithMessage('could_not_retrieve_comments', 404);
    }

    public function addTrackComment($trackId, Requests\TrackCommentRequest $request) {
        //You can only comment if the track is public or if you have unique url to the track
        $track = Track::where('id', $trackId)->first();
        if(is_null($track)) return $this->respondWithMessage('could_not_find_track', 404);

        //1 indicates Public / 0 indicates Private
        $isPublic = $track->public;
        $user = JWTAuth::parseToken()->authenticate();
        $currentUserId = $user->idu;

        if($isPublic==0 && ($currentUserId!=$track->uid))
            return $this->respondWithMessage("track_not_public", 404);

        $attributes = $request->all();
        $comment = new Comment();
        $comment->tid = $trackId;
        $comment->message = $attributes['message'];
        $comment->uid = $currentUserId;
        $comment->save();
        return $this->respondWithMessage("comment_added", 200);
    }

    public function deleteTrackComment($trackId, $commentId) {
        //You can only comment if the track is public or if you have unique url to the track
        //Only comment poster can delete the comment

        $track = Track::where('id', $trackId)->first();
        if(is_null($track)) return $this->respondWithMessage('could_not_find_track', 404);

        $comment = Comment::where('id', $commentId)->first();
        if(is_null($comment)) return $this->respondWithMessage('could_not_find_comment', 404);

        $user = JWTAuth::parseToken()->authenticate();
        $currentUserId = $user->idu;

        if($comment->uid!=$currentUserId)
            return $this->respondWithMessage("not_comment_owner", 400);

        $comment->delete();
        return $this->respondWithMessage("comment_deleted", 200);
    }



    /**
     * @SWG\Post(
     *     path="/tracks/{trackId}/ratings?token={token}",
     *     summary="Rate a track",
     *     tags={"tracks"},
     *     description="Rate a track",
     *     operationId="rate",
     *     parameters={},
     *     produces={"application/json"},
     *     @SWG\Parameter(name="token", in="path", type="string", required=true, description="Provide the access token of the user", maxLength=256),
     *     @SWG\Parameter(name="trackId", in="path", type="integer", required=true, description="Provide the id of the track to like", maxLength=256),
     *     @SWG\Response(
     *         response="200",
     *         description="Track Rated",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="message", type="string", description="track_rated"),
     *          )
     *     ),
     *     @SWG\Response(
     *         response="404",
     *         description="Invalid Rating Value",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="message", type="string", description="invalid_rating_provided"),
     *          )
     *     )
     * )
     * @param TrackLikeRequest|Requests\TrackRateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function rateTrack($trackId, Requests\TrackRateRequest $request){
        $user = JWTAuth::parseToken()->authenticate();
        $currentUserId = $user->idu;
        $attributes = $request->all();
        //Check whether the user is vip or not
        //If VIP then check whether the song is already rated by the user or not
        //If not rated check the range/value of rating
        //It it is 1,2,3,4,5 then update the rating
        //For rating increase the rating count in the track table
        //And do a entry in the vip_ratings table
        $rating = $attributes['rating'];
        if(!in_array($rating, [1, 2, 3, 4, 5])) return  $this->respondWithMessage("invalid_rating_provided", 401);
        $track = Track::where('id', $trackId)->first();
        if(is_null($track))    return $this->respondWithMessage("track_not_found", 403);
        $vipRating = VipRating::where('by', $currentUserId)->where('track', $trackId)->first();
        if(is_null($vipRating)) {
            $vipRating = new VipRating();
            $track->ratings = $track->ratings+$rating;
        } else  $track->ratings = $track->ratings+$rating-$vipRating->rating;
        $track->save();
        $vipRating->by = $currentUserId;
        $vipRating->track = $trackId;
        $vipRating->time = Carbon::now();
        $vipRating->rating = $rating;
        $vipRating->save();
        return $this->respondWithMessage("track_rated", 200);;
    }


    /**
     * @SWG\Delete(
     *     path="/tracks/{trackId}?token={token}",
     *     summary="Delete a track",
     *     tags={"tracks"},
     *     description="Delete a track",
     *     operationId="delete",
     *     parameters={},
     *     produces={"application/json"},
     *     @SWG\Parameter(name="token", in="path", type="string", required=true, description="Provide the access token of the user", maxLength=256),
     *     @SWG\Parameter(name="trackId", in="path", type="integer", required=true, description="Provide the id of the track to like", maxLength=256),
     *     @SWG\Response(
     *         response=200,
     *         description="Track deleted successfully",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\Items(message="track_delete_success")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="403",
     *         description="Delete Fail",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="message", type="string", description="track_delete_fail"),
     *          )
     *     ),
     *     @SWG\Response(
     *         response="404",
     *         description="Track Not Found",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="message", type="string", description="track_not_found"),
     *          )
     *     )
     * )
     * @param $trackId
     * @return \Illuminate\Http\JsonResponse
     * @internal param Requests\TrackDeleteRequest $request
     */

    public function deleteTrack($trackId) {
        $user = JWTAuth::parseToken()->authenticate();
        $currentUserId = $user->idu;
        $track = Track::where('uid', $currentUserId)->where('id', $trackId)->first();

        if(is_null($track)) return $this->respondWithMessage("track_not_found", 404);
        $trackName = $track->name;
        if(File::delete(storage_path().'/app/tracks/' . $trackName)){
            $track->delete();
            return $this->respondWithMessage("track_delete_success", 200);
        }
        return $this->respondWithMessage("track_delete_fail", 403);
    }

    public function getSelfTrack($trackId){
        $user = JWTAuth::parseToken()->authenticate();
        $currentUserId = $user->idu;
        $track = Track::where('uid', $currentUserId)->where('id', $trackId)->first();

        if(!is_null($track)) return $this->respondWithData($track);
        return $this->respondWithMessage("track_could_not_be_retrieved", 403);
    }

    public function getTrack($trackId){
        $track = Track::where('public', 1)->where('id', $trackId)->first();
        if(!is_null($track)) return $this->respondWithData($track);
        return $this->respondWithMessage("track_could_not_be_retrieved", 403);
    }

    /********************   Utility Functions Starts Here  ********************************/


//    public function respondWithData($data, $message) {
//        if(!is_array($data) && !is_null($data)) {
//            $data = $data->toArray();
//        }
//
//        if(is_null($data)){
//            return Response::json(['message' => $message], 404);
//        } else {
//            return Response::json(['data' => $data], 200);
//        }
//    }

    /**
     * @param $tracks
     * @param $message
     * @return \Illuminate\Http\JsonResponse
     */

    public function respondWithTracks($tracks, $message) {
        $tracks = (!is_array($tracks) && !is_null($tracks) ? $tracks = $tracks->toArray() : $tracks) ;
        return is_null($tracks)?$this->respondWithMessage($message, 404):$this->respondWithData($tracks);
    }


    /**
     * @param $data
     * @param int $statusCode
     * @return \Illuminate\Http\JsonResponse
     * @internal param int $errorCode
     */

    public function respondWithData($data, $statusCode=200) {
        return Response::json($data, $statusCode);
    }

    /**
     * @param $message
     * @param $code
     * @return \Illuminate\Http\JsonResponse
     */
    public function respondWithMessage($message, $code) {
        return Response::json(['message' => $message], $code);
    }

    private function getMaxTrackSizeAllowed($hasProAccount = false) {
        return $hasProAccount=="1"?env('PRO_MAX_TRACK_SIZE_ALLOWED_IN_MB'):env('GENERAL_MAX_TRACK_SIZE_ALLOWED_IN_MB');
    }

    private function getMaxSpaceAvailable($hasProAccount = false) {
        return $hasProAccount=="1"?env('PRO_MAX_SIZE_ALLOWED_IN_MB'):env('GENERAL_MAX_TRACK_SIZE_ALLOWED_IN_MB');
    }

    private function setTrackAttributes($track, $attributes) {
        if(array_key_exists('description', $attributes))    $track->description = $attributes['description'];
        if(array_key_exists('buyLink', $attributes))    $track->buy = $attributes['buyLink'];
        if(array_key_exists('recordLabel', $attributes))    $track->record = $attributes['recordLabel'];
        if(array_key_exists('releaseDate', $attributes))    $track->release = $attributes['releaseDate'];
        if(array_key_exists('license', $attributes))    $track->license = $attributes['license'];
        if(array_key_exists('download', $attributes))   $track->download = $attributes['download'];
        if(array_key_exists('embed', $attributes))  $track->public = $attributes['embed'];
        if(array_key_exists('title', $attributes))  $track->title = $attributes['title'];
        if(array_key_exists('tag', $attributes))  $track->tag = $attributes['tag'];
        $track->public = (array_key_exists('visibility', $attributes))?$attributes['visibility']:$track->public = 1;
        return $track;
    }

    public function resetTrackCounters($track) {
        $track->likes = 0;
        $track->downloads = 0;
        $track->views = 0;
        $track->ratings = 0;
        return $track;
    }

    private function checkForCopyRight($name) {
        $originalTrackPath = storage_path().'/app/tracks/'.$name;
        $sampleTrackPath = storage_path().'/app/sample/'.$name;

        $responseData = array();

        exec("ffmpeg -t 15 -i '$originalTrackPath' '$sampleTrackPath'", $output, $return);

        if (!$return) {
            //Sample Generated
            //Check for Copyright
            $http_method = "POST";
            $http_uri = "/v1/identify";
            $data_type = "audio";
            $signature_version = "1" ;
            $timestamp = time() ;

            $requrl = "http://ap-southeast-1.api.acrcloud.com/v1/identify";
            $access_key =  '3164b4a6266b59433b96b3680da16207';
            $access_secret =  'wUwAkLyeB1g9yINovYsbtWTnCiBrHb57fV8Lfl59';

            $string_to_sign = $http_method . "\n" .
                $http_uri ."\n" .
                $access_key . "\n" .
                $data_type . "\n" .
                $signature_version . "\n" .
                $timestamp;
            $signature = hash_hmac("sha1", $string_to_sign, $access_secret, true);
            $signature = base64_encode($signature);

            $file = $sampleTrackPath;
            $filesize = filesize($file);
            $cfile = new \CURLFile($file, "mp3", basename($name));

            $postfields = array(
                "sample" => $cfile,
                "sample_bytes"=>$filesize,
                "access_key"=>$access_key,
                "data_type"=>$data_type,
                "signature"=>$signature,
                "signature_version"=>$signature_version,
                "timestamp"=>$timestamp);

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $requrl);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);


            $response = curl_exec($ch);
            $responseDecoded = json_decode($response, 1);

            if(curl_error($ch)) {
                return 0;
            }
            $responseCode = $responseDecoded['status']['code'];
            curl_close($ch);
            if ($responseCode == 0) {
                $responseData['message'] = "copyrighted_track";
                $responseData['label'] =
                    isset($responseDecoded['metadata']['music'][0]['label'])?$responseDecoded['metadata']['music'][0]['label']:"";
                $responseData['title'] =
                    isset($responseDecoded['metadata']['music'][0]['title'])?$responseDecoded['metadata']['music'][0]['title']:"";
                $responseData['album'] =
                    isset($responseDecoded['metadata']['music'][0]['album']['name'])?$responseDecoded['metadata']['music'][0]['album']['name']:"";
                return $responseData;
            } else if($responseCode == 1001){
                return 1;
            }
        }

        $responseData['message'] = "error_checking_for_copyright";
        return $responseData;
    }

    private function compressAndSaveImage($source, $destination, $quality = 75) {
        $info = getimagesize($source);
        $image = null;
        if ($info['mime'] == 'image/jpeg')
            $image = imagecreatefromjpeg($source);

        elseif ($info['mime'] == 'image/gif')
            $image = imagecreatefromgif($source);

        elseif ($info['mime'] == 'image/png')
            $image = imagecreatefrompng($source);
        else
            return "";

        imagejpeg($image, $destination, $quality);

        return $destination;
    }

    /********************   Utility Functions Ends Here    ********************************/



}
