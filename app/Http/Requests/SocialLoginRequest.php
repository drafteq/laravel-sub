<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Http\JsonResponse;


class SocialLoginRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'apiKey' => 'string|required',
            'email'  => 'string|required',
            'secret'  => 'string|required',
            'socialType'  => 'string|required',
            'onesignalId' => 'string'
        ];
    }

    public function response(array $errors) {
        return new JsonResponse($errors, 422);
    }
}
