<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Http\JsonResponse;

class UserRegistrationRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username'  =>  'string|required',
            'password'  =>  'string|required',
            'email'  =>  'string|required',
            'apiKey' => 'string|required'
        ];
    }
    public function response(array $errors) {
        return new JsonResponse($errors, 422);
    }

}
