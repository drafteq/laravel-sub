<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Http\JsonResponse;


class UserGeneralSettingUpdateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstName' =>  'string',
            'lastName'  =>  'string',
            'country'  =>  'string',
            'city'  =>  'string',
            'website'  =>  'string',
            'description'  =>  'string',
            'chatStatus'  =>  'integer',
            'profile'  =>  'integer',
        ];
    }

    public function response(array $errors) {
        return new JsonResponse($errors, 422);
    }
}
