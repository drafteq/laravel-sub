<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Http\JsonResponse;

class TrackUploadRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "title"         =>  "required",
            "tag"           =>  "required|string",
            "description"   => 'string',
            "name"          =>  "required|string",
            "art"           =>  "string",
            "buyLink"       =>  "string",
            "recordLabel"   =>  "string",
            "releaseDate"   =>  "date",
            "license"       =>  "integer",
            "download"      =>  "integer",
            "visibility"    =>  "integer",
            "embed"         =>  "string"
        ];
    }

    public function response(array $errors) {
        return new JsonResponse($errors, 422);
    }


}
