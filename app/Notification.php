<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Notification
 *
 * @package App
 *
 * @SWG\Definition(
 *   definition="Notification",
 *   required={"name"}
 * )
 *
 */

class Notification extends Model
{

    /**
     *
     *  @SWG\Property(property="id", type="string", required=true),
     *  @SWG\Property(property="from", type="integer", required=true),
     *  @SWG\Property(property="to", type="integer", required=true),
     *  @SWG\Property(property="parent", type="integer", required=true),
     *  @SWG\Property(property="child", type="integer", required=true),
     *  @SWG\Property(property="type", type="integer", required=true),
     *  @SWG\Property(property="read", type="integer", required=true),
     *  @SWG\Property(property="time", type="date-time", required=true),
     */

    protected $fillable  = ['read'];
    public $timestamps = false;

    public function track() {
        return $this->belongsTo('App\Track', 'parent');
    }

    public function byUser(){
        return $this->belongsTo('App\User', 'from', 'idu');
    }
}
