<?php

namespace App;

use Carbon\Carbon;
use URL;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Track
 *
 * @package App
 *
 * @SWG\Definition(
 *   definition="Category",
 *   required={"id"}
 * )
 *
 */

class Category extends Model {

    protected $table = 'categories';
    /**
     *
     *  @SWG\Property(property="id", type="integer", required=true),
     *  @SWG\Property(property="name", type="string", required=true),
     */

    public $timestamps = false;

}
